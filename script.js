/*
Методы объекта это функции, которые хранятся в Объекте как его и остальные свойства. Отличие их от обычных функций в том, что вызов метода связан с именем объекта, и он может работать с данными содержащимися в объекте.
*/
const newUser = CreateNewUser();
console.log(newUser.getLogin());

function CreateNewUser() {
  firstName = prompt("Enter your name, please");
  lastName = prompt("Enter your last name, please");
  const user = {
    firstName,
    lastName,
    getLogin: function () {
      return (this.firstName[0] + this.lastName).toLowerCase();
    },
    setFirstName: function (newFName) {
      Object.defineProperty(newUser, "firstName", {
        value: newFName,
        writable: false,
        configurable: true,
      });
    },
    setLastName: function (newLName) {
      Object.defineProperty(newUser, "lastName", {
        value: newLName,
        writable: false,
        configurable: true,
      });
    },
  };
  return user;
}

Object.defineProperty(newUser, "firstName", {
  value: firstName,
  writable: false,
  configurable: true,
});
Object.defineProperty(newUser, "lastName", {
  value: lastName,
  writable: false,
  configurable: true,
});
